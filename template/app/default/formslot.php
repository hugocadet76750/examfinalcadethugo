<form action="" method="post" novalidate class="wrapform">
    <?php echo $form->label('Room'); ?>
    <?php echo $form->select('room', $room, 'title', $slot->id_room ?? ''); ?>
    <?php echo $form->error('room'); ?>
    <?php echo '<br>'; ?>
    <?php echo $form->label('Date of start'); ?>
    <?php echo $form->input('start_at','datetime-local'); ?>
    <?php echo $form->error('start_at'); ?>
    <?php echo '<br>'; ?>
    <?php echo $form->label('Number of hours'); ?>
    <?php echo $form->input('nbr_hours','number'); ?>
    <?php echo $form->error('nbr_hours'); ?>
    <?php echo '<br>'; ?>
    <?php echo $form->submit('submitted', $textButton); ?>
</form>