<form action="" method="post" novalidate class="wrapform">
    <?php echo $form->label('Title'); ?>
    <?php echo $form->input('title') ?>
    <?php echo $form->error('title'); ?>
    <?php echo '<br>'; ?>
    <?php echo $form->label('Maximum of users'); ?>
    <?php echo $form->input('maxuser','number'); ?>
    <?php echo $form->error('maxuser'); ?>
    <?php echo '<br>'; ?>
    <?php echo $form->submit('submitted', $textButton); ?>
</form>