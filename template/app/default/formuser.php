<form action="" method="post" novalidate class="wrapform">
    <?php echo $form->label('Name'); ?>
    <?php echo $form->input('name') ?>
    <?php echo $form->error('name'); ?>
    <?php echo '<br>'; ?>
    <?php echo $form->label('Email'); ?>
    <?php echo $form->input('email','email'); ?>
    <?php echo $form->error('email'); ?>
    <?php echo '<br>'; ?>
    <?php echo $form->submit('submitted', $textButton); ?>
</form>