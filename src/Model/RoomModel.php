<?php

namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;


class RoomModel extends AbstractModel
{
    protected static $table = 'room';
    protected int $id;
    protected string $title;
    protected int $maxuser;

    public static function allBy($type,$order)
    {
        return App::getDatabase()->query("SELECT * FROM ".self::getTable(). " ORDER BY $type $order" ,get_called_class());
    }

    public static function findById($id,$columId = 'id')
    {
        return App::getDatabase()->prepare("SELECT * FROM " . self::getTable() . " WHERE ".$columId." = ?",[$id],get_called_class(),true);
    }

    public static function insert($post){
        App::getDatabase()->prepareInsert("INSERT INTO " . self::getTable() . " (title,maxuser) VALUES (?,?)",array($post['title'],$post['maxuser']));
    }


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }


    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }


    /**
     * @return int
     */
    public function getMaxuser(): int
    {
        return $this->maxuser;
    }
}