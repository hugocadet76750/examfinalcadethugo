<?php

namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;


class SlotModel extends AbstractModel
{
    protected static $table = 'slot';
    protected int $id;
    protected int $title;
    protected int $id_room;
    protected int $nbr_hours;
    protected $start_at;

    public static function allBy($type,$order)
    {
        return App::getDatabase()->query("SELECT * FROM ".self::getTable(). " ORDER BY $type $order" ,get_called_class());
    }

    public static function allSlotsBy($type,$order)
    {
        return App::getDatabase()->query("SELECT s.id, s.id_room, r.title FROM ".self::getTable(). " AS s LEFT JOIN room AS r ON r.id = s.id_room ORDER BY $type $order" ,get_called_class());
    }

    public static function findById($id,$columId = 'id')
    {
        return App::getDatabase()->prepare("SELECT * FROM " . self::getTable() . " WHERE ".$columId." = ?",[$id],get_called_class(),true);
    }

    public static function insert($post)
    {
        App::getDatabase()->prepareInsert("INSERT INTO " . self::$table . " (id_room,start_at,nbr_hours) VALUES (?,?,?)",array($post['room'],$post['start_at'],$post['nbr_hours']));
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getIdRoom(): int
    {
        return $this->id_room;
    }

    /**
     * @return int
     */
    public function getNbrHours(): int
    {
        return $this->nbr_hours;
    }

    /**
     * @return mixed
     */
    public function getStartAt()
    {
        return $this->start_at;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }



}