<?php

namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;


class UserModel extends AbstractModel
{
    protected static $table = 'user';
    protected int $id;
    protected string $name;
    protected string $email;

    public static function allBy($type,$order)
    {
        return App::getDatabase()->query("SELECT * FROM ".self::getTable(). " ORDER BY $type $order" ,get_called_class());
    }

    public static function insert($post){
        App::getDatabase()->prepareInsert("INSERT INTO " . self::getTable() . " (name,email) VALUES (?,?)",array($post['name'],$post['email']));
    }

    public static function selectEmail($post)
    {
        return App::getDatabase()->prepare("SELECT * FROM ".self::getTable(). " WHERE email = ?" ,array($post['email']), get_called_class(), true);
    }


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

}