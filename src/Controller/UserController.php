<?php

namespace App\Controller;
use App\Model\UserModel;
use App\Service\Form;
use App\Service\Validation;
use Core\Kernel\AbstractController;

/**
 *
 */
class UserController extends AbstractController
{

    public function adduser()
    {
        $textButton = 'ADD';
        $message = 'ADD A USER';
        $errors = array();
        if(!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            $v = new Validation();
            $errors = $this->validate($v,$post);
            $existing = UserModel::selectEmail($post);
            if (empty($errors['email'])) {
                if (!empty($existing)) {
                    $errors['email'] = 'This mail already exists';
                }
                $validation = new Validation();
                $this->validate($validation, $post);
                if ($validation->IsValid($errors)) {
                    UserModel::insert($post);
                    $this->redirect('listing-users');
                }
            }
        }
        $form = new Form($errors);

        $this->render('app.default.add-user',array(
            'message' => $message,
            'textButton' => $textButton,
            'form' => $form,
        ));
    }

    public function listinguser()
    {
        $user = UserModel::allBy('id','DESC');

        $message = 'Listing';
        $this->render('app.default.listing-user',array(
            'user' => $user,
            'message' => $message,
        ));
    }

    private function validate($v,$post)
    {
        $errors = [];
        $errors['name'] = $v->textValid($post['name'], 'name',2, 150);
        $errors['email'] = $v->textValid($post['email'], 'email',5, 150);
        return $errors;
    }
}
