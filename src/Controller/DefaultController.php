<?php

namespace App\Controller;

use Core\Kernel\AbstractController;

/**
 *
 */
class DefaultController extends AbstractController
{
    public function index()
    {
        $message = 'MVC Examen Final CADET Hugo';
        $this->render('app.default.frontpage',array(
            'message' => $message,
        ));
    }

    public function listing()
    {
        $message = 'LISTING';
        $this->render('app.default.listing',array(
            'message' => $message,
        ));
    }

    /**
     * Ne pas enlever
     */
    public function Page404()
    {
        $this->render('app.default.404');
    }
}
