<?php

namespace App\Controller;
use App\Model\RoomModel;
use App\Service\Form;
use App\Service\Validation;

use Core\Kernel\AbstractController;

/**
 *
 */
class RoomController extends AbstractController
{

    public function addroom()
    {
        $textButton = 'ADD';
        $message = 'ADD A ROOM';
        $errors = [];
        if (!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            $validation = new Validation();
            $errors = $this->validate($validation, $post);
            if ($validation->IsValid($errors)) {
                RoomModel::insert($post);
                $this->redirect('listing-users');
            }
        }
        $form = new Form($errors);

        $this->render('app.default.add-room',array(
            'message' => $message,
            'textButton' => $textButton,
            'form' => $form,
        ));
    }

    public function listingroom()
    {
        $room = RoomModel::allBy('id','DESC');

        $message = 'Listing';
        $this->render('app.default.listing-room',array(
            'room' => $room,
            'message' => $message,
        ));
    }


    private function validate($v,$post)
    {
        $errors = [];
        $errors['title'] = $v->textValid($post['title'], 'title',2, 150);
        $errors['maxuser'] = $v->textValid($post['maxuser'], 'maxuser',1, 3);
        return $errors;
    }

}
