<?php

namespace App\Controller;
use App\Model\RoomModel;
use App\Model\SlotModel;
use App\Service\Form;
use App\Service\Validation;

use Core\Kernel\AbstractController;

/**
 *
 */
class SlotController extends AbstractController
{

    public function addslot()
    {
        $textButton = 'SCHEDULE';
        $message = 'SCHEDULE A SLOT';
        $room = RoomModel::allBy('id','DESC');

        $errors = [];
        if (!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            $validation = new Validation();
            $errors = $this->validate($validation, $post);
            if ($validation->IsValid($errors)) {
                SlotModel::insert($post);
                $this->redirect('listing-slot');
            }
        }
        $form = new Form($errors);

        $this->render('app.default.add-slot',array(
            'message' => $message,
            'form' => $form,
            'room' => $room,
            'textButton' => $textButton,
        ));
    }

    public function listingslot()
    {
        $slot = SlotModel::allBy('id','DESC');
        $room = RoomModel::allBy('id','DESC');

        $message = 'Listing';
        $this->render('app.default.listing-slot',array(
            'slot' => $slot,
            'romm' => $room,
            'message' => $message,
        ));
    }

    public function singleslot($id)
    {
        $uniqueslot = $this->getSlotByIdOr404($id);
        $this->render('app.default.single-slot', array(
            'uniqueslot' => $uniqueslot,
        ));
    }

    private function validate($v,$post)
    {
        $errors = [];
        $verifroom = RoomModel::findById($post['room']);
        if(empty($verifroom)) {
            $errors['subs'] = 'Error';
        }
        if(empty($post['start_at'])) {
            $errors['start_at'] = 'Choose a date';
        }
        $errors['nbr_hours'] = $v->textValid($post['nbr_hours'], 'nbr_hours',1, 3);
        return $errors;
    }

    private function getSlotByIdOr404($id){
        $slot = SlotModel::findById($id);
        if(empty($slot)) {
            $this->Abort404();
        }
        return $slot;
    }
}
